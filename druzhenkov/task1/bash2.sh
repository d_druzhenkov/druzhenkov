#!/bin/bash

if [ -n "$1" ] #check first parameter
then
	dir=`pwd`
	var=`find $dir -name $1 -type f` #find file in current dir  
        if [ $var > 0 ] # check name first parameter
	then	
		echo "Configuration file exists. "
		flag=1
	else
                echo "Configuration file not found. Check name and try again. "  
        fi
	
else

	echo "No parameter found. Set first parameter (name configuration file). "
fi
if [ -n "$2" ] #check second parameter
then
	echo "Output file is defined as $2"
	flag2=1
else
	echo "No second parameter. Set parameter. "
fi
if [ $flag = 1 ] && [ $flag = 1 ] #if parameters set to run command
then
	result_file=${dir}/$2 #set dir for old file
	rm $result_file #rm old file
	while read -r LINE; do #read file line by line and find patterns
	     
		case $LINE in
		HOME)
		echo $HOME >> $2
		;;
	        PWD)
		echo $PWD >> $2
	        ;;
	        USER)
		echo $USER >> $2
	        ;;
	        LANG)
		echo $LANG >> $2
	        ;;
	        SHELL)
		echo $SHELL >> $2
		;;
		TERM)
		echo $TERM >> $2
		;;
	        dir*)			
		echo $LINE | awk '{print $2}' >> $2
		;;
	        file*)
		echo $LINE | awk '{print $2}' >> $2
		;;
	        *)
		echo $NaN >> $2	
		;;
	        esac    

	done < $1
fi
