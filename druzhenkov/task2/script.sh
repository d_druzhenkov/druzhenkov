#!/bin/bash
while :
do
echo "Бесконечный цикл нажмите CNTRL+C для выхода"
echo "Идет чтение директории"
sleep 3
dir=`pwd` #get current dir
config_file=${dir}/config.txt #get path to config file
current_dir=`sed -n -e '1p' $config_file | awk '{print $2}'` #read path of the current dir
result_dir=`sed -n -e '2p' $config_file | awk '{print $2}'` #read path of the result dir
find $current_dir -type f | grep -v copyx > tmp_s1 # find new files without alredy copied and write the path to the file tmp_s1
val2="copyx" # write in variable val2 copyx
while read -r LINE; do # read file tmp_s1 line by line   

   val1=`sed -n -e '1{/copyx/p}' "$LINE" | grep -o 'copyx'` # write in variable val1 result sed and grep command (find copyx)
   if [ $val1 = "$val2" ] # check if have copyx in first line thus already use another copy programe    
       then
       echo "already use another copy programe"
       else	   
       echo "start"
       name_file=`sed -n -e '1p' $LINE | awk '{print $1}'` #write in var name file
       time_file=`sed -n -e '1p' $LINE | awk '{print $2}'` #write in var time file
       sed -i ' 1 s/.*/&copyx/' $LINE # add key copyx for "freeze" that file from another programe
       UUID=`cat $LINE | tail -n 1 | egrep -e '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}'` #write in variable UUID last line if found UUID
       
       if [ $UUID > 0 ] # if found UUID then copy and rename file 
              then
              echo "Found UUID. Copy and rename that file"
              LINE2=${LINE}copyx #get nem name file    
              mv "$LINE" "$LINE2" #rename file with copyx
	      #path=/home/ubuntu/check/
              cur_date="$(date +%Y-%m-%d-%H-%M)" #get current date
	      LASTNAME=${result_dir}${name_file}_${time_file}_${cur_date} #create name and path for new file
	      cp "$LINE2" "$LASTNAME" #copy file to new dir
              sed -i -e '1{s/copyx//}' $LINE2 #delete copyx from the end of first line
              sed -i -e '1{s/copyx//}' $LASTNAME #delete copyx form the end of first line               
	      Log=${cur_date}\;${LINE}\;copyto\;${LASTNAME} #create log
	      echo $Log >> logging #write to logging file

       else        # if not found UUID then remove "freeze that file" 
	       echo "UUID not found"
	       sed -i -e '1{s/copyx//}' $LINE # delete copyx from the end of first line 
       fi

   fi   
done < tmp_s1
done
